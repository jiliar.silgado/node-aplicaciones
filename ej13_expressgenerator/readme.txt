0. Instalación de express-generator
npm install express-generator -g
Nota: La instalación se hace global por esto la -g

1. Creación de Aplicación:
express --view=nombre_view_engine nombre_aplicacion
Ej1:
express --view=pug pug-generator
Ej2:
express --view=hbs hbs-generator

2. Instalación de modulos con npm i

3. Ejecución de app segun Sistema Operativo:

En MacOS o Linux, ejecute la aplicación con este mandato:
$ DEBUG=nombre_app:* npm start

En Windows, utilice este mandato:
> set DEBUG=nombre_app:* & npm start

nombre_app = Nombre de la Aplicación

Ir a: http://localhost:3000/