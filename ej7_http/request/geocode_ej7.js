//https://maps.googleapis.com/maps/api/geocode/json?address=Cartagena%20de%20Indias+CA&key=AIzaSyBLMa7emvhGQMK0uYYrS-9aymoEhlX-4sU
const argv =  require('yargs').argv;
const request = require('request');

let direccion = argv.direccion;
let key = 'AIzaSyBLMa7emvhGQMK0uYYrS-9aymoEhlX-4sU';
let url = `https://maps.googleapis.com/maps/api/geocode/json?address=${direccion}+CA&key=${key}`

request({
    url: url,
    json: true,
  },(error, response, body)=>{
    if(error){
      console.log('Servicio no disponible');
    }else if(body.status === 'ZERO_RESULTS'){
      console.log('No hay datos para mostrar');
    }else if(body.status === 'OK'){
      var body_string =  JSON.stringify(body, undefined, 1);
      console.log(body_string);
      console.log(JSON.stringify(`Ubicación : ${body.results[0].formatted_address}`));
      console.log(JSON.stringify(`Latitud : ${body.results[0].geometry.location.lat}`));
      console.log(JSON.stringify(`Longitud : ${body.results[0].geometry.location.lng}`));
    }
  });

/*request('http://www.google.com', function (error, response, body) {
  console.log('error:', error); // Print the error if one occurred
  console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
  console.log('body:', body); // Print the HTML for the Google homepage.
});*/