const https = require('https');

let username = 'Jiliar';

//http://www.useragentstring.com/index.php?id=19937
let USER_AGENT_FIREFOX = 'Mozilla/5.0 (X11; Linux i686; rv:64.0) Gecko/20100101 Firefox/64.0';
let USER_AGENT_CHROME = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36';
let USER_AGENT_IE = 'Mozilla/5.0 (Windows NT 6.1; WOW64; Trident/7.0; AS; rv:11.0) like Gecko';
let USER_AGENT_EDGE = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML like Gecko) Chrome/51.0.2704.79 Safari/537.36 Edge/14.14931';
//Cliente en un protocolo de red.

let options = {
    host: 'api.github.com',
    path: '/users/'+username,
    method: 'GET',
    headers: {'user-agent':USER_AGENT_CHROME}
}

let request = https.request(options, (response)=>{
                let body = '';
                response.on('data', (out)=>{
                    body += out;
                });

                response.on('end', (out)=>{
                    let json = JSON.parse(body);
                    console.log(json);
                });
            });

request.on('error', (e)=>{
    console.log(e);
});

request.end();