let calcular = (numero1, numero2)=>{
    return new Promise((resolve, reject)=>{
        setTimeout(()=>{
            let suma = numero1 + numero2;
            if(suma == 12){
                resolve(suma);
            }else{
                reject('Error en el procesamiento de datos, la suma no es igual a 12');
            }
        },2000);
    });
}

module.exports = {
    //calcular: calcular // Si la función tiene el mismo nombre de la variable a llamar se coloca solo el nombre
    calculo: calcular
}