//Ejemplo A
let promesa = new Promise((resolve, reject)=>{
                resolve('Ejemplo 1: Exito al Procesar los datos');
                //reject('Error'); //Para Testear Reject quite comentarios y comente la  línea anterior 
});

promesa.then((resultado)=>{
    console.log(resultado);
},(error)=>{
    console.log(error);
});

//Ejemplo B
let promesa2 =require('./modulo_ext_ej8.js');
promesa2.calculo(5, 8).then((resultado)=>{
    console.log('Ejemplo 2: '+resultado);
},(error)=>{
    console.log('Error en tiempo de ejecución: '+error);
});
