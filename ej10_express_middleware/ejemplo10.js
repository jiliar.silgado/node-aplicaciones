const express = require('express');
const app = express();

//CREACIONES DE MIDDLEWARE
let isLogin = true;

//Middleware 2: Para auditoria de Peticiones
let logger = (req, res, next)=>{
    console.log(`Petición de tipo: ${req.method}`);
    next(); //Llama al siguiente Middleware Responsable (Middleware 2)
};

//Middleware 3: Para visualizar de que IP provienen las Peticiones
let showIP = (req, res, next)=>{
    console.log('IP: 127.0.0.1');
    next(); //Llama al siguiente Middleware Responsable (Middleware 3)
};

//Middleware 1: Para verificación de Loggeo de Aplicación

app.use((req, res, next)=>{
    if(isLogin){
        next(); //Ejecuta el Middleware responsable
    }else{
        res.send('No estas Loggeado');
    }
},
logger,
showIP
);

//Otro modo de incluir Middleware
//app.use(logger);
//app.use(showIP);

//Para probar los tipos de metodos se puede utilizar Postman


//UTILIZACIÓN DE EXPRESS

//Enviar Usuario por URL
app.get('/:user', (req, res)=> {
    let usuario = req.params.user;
    res.send(`Bienvenido Usuario ${usuario}`);
  });

  //Sin Enviar Usuario por URL
/*app.get('/', (req, res)=> {
  res.send(`hello world ${req.method}`);
});*/

app.post('/', (req, res)=> {
    res.send(`hello world ${req.method}`);
});

app.put('/', (req, res)=> {
    res.send(`hello world ${req.method}`);
});

app.delete('/', (req, res)=> {
    res.send(`hello world ${req.method}`);
});
  
app.listen(3000, ()=>{
    console.log('La aplicación esta escuchando por el puerto 3000');
});

//Ejecución que se realiza hasta antes que regrese la petición hacia el BhxBrowser, es decir, antes del Response.
