1. Se instala express-generator

2. Se crea aplicacion: express --view=hbs crud-generator

3. Se instalan modulos npm i

4. Se crean los siguientes directorios y ficheros con su debida configuración:
config>conexion.js
models>persona.js (Según entidad que se desee crear)

5. Instalar mongoose: npm i mongoose --save

6. Se modifica archivo app.js
creando variables de rutas segun sean las entidades, ej:
var personaForm = require('./routes/personaForm');
app.use('/personas', personaForm);

7. Se modifican views:
7.1. layout.hbs
7.2. index.hbs
7.3. Se crean views de entidades.

8. Modificación de archivo index.js

9. Se crea archivo entidadForm.hbs para gestionar vista de registro de datos, ejemplo:
personaForm.hbs

10. Se crea archivo entidadForm.js para gestionar envio de datos del formulario, ejemplo:
personaForm.js

11. Ejecutar aplicacion:
En MacOS o Linux, ejecute la aplicación con este mandato:
$ DEBUG=nombre_app:* npm start

En Windows, utilice este mandato:
> set DEBUG=nombre_app:* & npm start