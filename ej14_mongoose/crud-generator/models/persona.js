let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let personaSchema = new Schema({
    id: { type: String },
    nombre1: { type: String },
    nombre2: { type: String },
    apellido1: { type: String },
    apellido2: { type: String },
    direccion: { type: String },
    email: { type: String },
    telefono: { type: String },
    sexo: { type: String },
    edad: { type: Number, min: 0 }
}, { versionKey: false });

let Persona = mongoose.model('personas', personaSchema);

module.exports = Persona;