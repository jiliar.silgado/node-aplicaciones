let express = require('express');
let router = express.Router();

let mongoose = require('../config/conexion');
let Persona = require('../models/persona');

router.post('/persona/operar', (req, res, next) => {
  console.log(req.body);  

  if (req.body._id === "") {
    let per = new Persona({
      id: req.body.identificacion,
      nombre1: req.body.nombre1,
      nombre2: req.body.nombre2,
      apellido1: req.body.apellido1,
      apellido2: req.body.apellido2,
      direccion: req.body.direccion,
      email: req.body.email,
      telefono: req.body.telefono,
      sexo: req.body.sexo,
      edad: req.body.edad
    });
    
    per.save();
  } else {
    Persona.findByIdAndUpdate(req.body._id, { $set: req.body }, { new: true }, (err, model) => {
      if (err) throw err;
    });
  }
  res.redirect('/');
});

module.exports = router;