//Funciones Flechas apartir de ES6

module.exports={
    suma: (a,b)=>{return a + b;},// No se necesita la palabra function. (Para operaciones con mas de una línea de codigo)
    resta: (a,b)=> a - b, //No se necesitan llaves. (Para operaciones sencillas)
    multiplicarByCinco: a => a * 5, //No se necesitan parentesis para un solo valor (Para operaciones sencillas)
    division: (a,b)=> a/b
}