//npm init para crear archivo package.json
//instalación de modulos npm i nombre_modulo --save
//portabilidad de dependencias en node.js: luego de desacargar de cualqueir repositorio con npm -i se reinstalaran dependencias.

//Implementación de Lodash: Gestión de JSON
const _ = require('lodash');

let x = {'nombre':'Jiliar'};
let y = {'apodo':'JianSyStyle'};
let z = [
    {nombre: 'Maria', apellido: 'Puerta', edad: '32'},
    {nombre: 'Ariel', apellido: 'Puerta', edad: '23'}
];

//Concatenación de JSON
console.log('------------------------------');
let resultado = _.assign(x,y);
console.log(resultado);
console.log('------------------------------');

//Llamar una función n veces
_.times(3, ()=>console.log('Suscribete'));
console.log('------------------------------');

//Encontrar un dato en JSON
let res = _.find(z,{nombre:'Maria'});
console.log(res);
console.log('------------------------------');

//Captura de datos por consola (Nativo)
// Apartir de la posición dos se reciben los parametros insertados
// Comando para ejecución del codigo node ejemplo5.js 'usuario' 'buscar' 'Ariel'
let comando = process.argv[2]; 
let funcion = process.argv[3]; 
if(comando === 'usuario'){
    if(funcion === 'buscar'){
        let nom = process.argv[4]; 
        let res = _.find(z,{nombre:nom});
        console.log(res);
        console.log('------------------------------');
    }
}else{
    console.log('Operacion no procesada');
    console.log('------------------------------');
}

//Captura de datos por consola (modulo yargs)
//npm i yargs --save
//node ejemplo5.js --usuario=funcionario --funcion=buscar --nombre=Maria
const argv = require('yargs').argv;
if(argv.usuario === 'funcionario'){
    if(argv.funcion === 'buscar'){
        let nom = argv.nombre; 
        let res = _.find(z,{nombre:nom});
        console.log(res);
        console.log('------------------------------');
    }
}else{
    console.log('Operacion no procesada');
    console.log('------------------------------');
}

