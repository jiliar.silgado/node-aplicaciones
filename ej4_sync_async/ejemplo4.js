//Lectura de Archivos
const fs = require('fs');

//Lectura de datos - Proceso Asincrono
/*fs.readFile('codetest_ej4.txt', 'utf-8', (error, data)=>{
    if(error){
        console.log(`Se ha generado un error ${error}`);
    }else{
        console.log(data);
    }
});*/

//Lectura de datos - Proceso Sincrono
fs.readFileSync('codetest_ej4.txt', 'utf-8', (error, data)=>{
    if(error){
        console.log(`Se ha generado un error ${error}`);
    }else{
        console.log(data);
    }
});

//Renombrar archivo - Proceso Asincrono
/*fs.rename('codetest_ej4.txt', 'codetestrenombrado_ej4.txt', (error)=>{
    if(error)throw error;
        console.log('¡Renombrado!');
});*/

//Renombrar archivo - Proceso Sincrono
fs.renameSync('codetest_ej4.txt', 'codetestrenombrado_ej4.txt', (error)=>{
    if(error)throw error;
        console.log('¡Renombrado!');
});

//Agregar texto - Proceso Asincrono
/*fs.appendFile('codetestrenombrado_ej4.txt', '\n Archivo ha sido renombrado', (error)=>{
    if(error)
        console.log(`Se ha generado un error ${error}`);
});*/

//Agregar texto - Proceso Sincrono
fs.appendFileSync('codetestrenombrado_ej4.txt', '\n Archivo ha sido renombrado', (error)=>{
    if(error)
        console.log(`Se ha generado un error ${error}`);
});


//Copiar archivo - Proceso Asincrono
/*fs.createReadStream('codetestrenombrado_ej4.txt').pipe(fs.createWriteStream('codetest_ej4.txt'));*/

//Copiar archivo - Proceso Sincrono
fs.createReadStream('codetestrenombrado_ej4.txt').pipe(fs.createWriteStream('codetest_ej4.txt'));

//Elminar archivo - Proceso Asincrono
/*fs.unlink('codetestrenombrado_ej4_eliminar.txt', (error)=>{
    if(error) throw error;
        console.log('Eliminado');
});*/

//Elminar archivo - Proceso Sincrono
fs.unlinkSync('codetestrenombrado_ej4.txt', (error)=>{
    if(error) throw error;
        console.log('Eliminado');
});

//Lectura de archivos en Directorios - Proceso Asincrono
/*fs.readdir("C:/Users/dell/Documents/NodeJS/node-tutos/directorio_ej4/file_ej4.txt", (error, files)=>{
    files.forEach(file=>{
        console.log(file)
    });
});*/

//Lectura de archivos en Directorios - Proceso Sincrono
fs.readdirSync("./directorio_ej4").forEach(file=>{
        console.log(file);
});


//CONCEPTOS
//Procesamiento Asincrono: permite ejecutar diferentes operaciones simultaneamente en un espacio de tiempo.
//Procesamiento Sincrono: Las operaciones ejecutadas deben estar uno detras del otro, es decir, secuencialmente.
//Node.js trabaja asincronamente por defecto. 