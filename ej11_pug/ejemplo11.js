const express = require('express');
const app = express();

let personas = [
    {id:1, nombre1:'Jiliar', apellido1:'Silgado', apellido2:'Cardona', telefono:'3016733590'},
    {id:2, nombre1:'Maria', apellido1:'Puerta', apellido2:'Salas', telefono:'3045391875'},
    {id:3, nombre1:'Pablo', apellido1:'Guerrero', apellido2:'Figueroa', telefono:'3016733590'},
    {id:4, nombre1:'Roy', apellido1:'Blanquiceth', apellido2:'Guerrero', telefono:'3016733590'},
    {id:5, nombre1:'Duban', apellido1:'Silgado', apellido2:'Cardona', telefono:'3016733590'},
];

//Creación de Plantillas con pug
app.set('view engine', 'pug');

app.get('/', (req, res)=>{
    res.render('template', {titulo: 'Titulo Pug',
                            mensaje: 'Pruebas de renderización de Plantilla | Pug',
                            personas});
});

app.listen(3000, ()=>{
    console.log('La aplicación esta escuchando por el puerto 3000');
});
